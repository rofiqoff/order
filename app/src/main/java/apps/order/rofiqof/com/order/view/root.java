package apps.order.rofiqof.com.order.view;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import apps.order.rofiqof.com.order.R;
import apps.order.rofiqof.com.order.modules.WireframeClass;

public class root extends AppCompatActivity {

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.root);

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                WireframeClass.getInstance().toLoginView(root.this);
                finish();
            }
        }, 3000);
    }
}
