package apps.order.rofiqof.com.order.utils;

import apps.order.rofiqof.com.order.Entity.model.Login.Login;
import apps.order.rofiqof.com.order.Entity.model.LupaPassword.LupaPassword;
import apps.order.rofiqof.com.order.Entity.model.Register.Register;
import apps.order.rofiqof.com.order.support.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by rofiqoff on 9/17/17.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("register")
    Call<Register> register (
            @Field("nohandphone") String nohandphone,
            @Field("password") String password,
            @Field("imei") String imei,
            @Field("email") String email,
            @Field("latitudeasli") float latitudeasli,
            @Field("longitudeasli") float longitudeasli,
            @Field("alamat") String alamat,
            @Field("propinsi") String propinsi,
            @Field("namauser") String namauser
    );

    @FormUrlEncoded
    @POST("login")
    Call<Login> login (@Field("username") String username,
                       @Field("password") String password);


    @FormUrlEncoded
    @POST("gantipassword")
    Call<LupaPassword> gantiPassword(
            @Field("username") String userame,
            @Field("imei") String imei,
            @Field("password_baru") String passwordBaru
    );

    class Factory {
        public static ApiService create() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ServiceGenerator.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(ApiService.class);
        }
    }
}
