package apps.order.rofiqof.com.order.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import apps.order.rofiqof.com.order.BuildConfig;
import apps.order.rofiqof.com.order.Entity.model.Login.Login;
import apps.order.rofiqof.com.order.Entity.model.Login.Success;
import apps.order.rofiqof.com.order.R;
import apps.order.rofiqof.com.order.databinding.ActivityLoginBinding;
import apps.order.rofiqof.com.order.modules.WireframeClass;
import apps.order.rofiqof.com.order.support.ServiceGenerator;
import apps.order.rofiqof.com.order.utils.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginView extends AppCompatActivity {

    ActivityLoginBinding content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_login);
        content.setView(this);

    }

    public void loginClick() {
        if (content.username.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(LoginView.this, "Email/No.Handphone tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else if (content.password.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(LoginView.this, "Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else {
            content.loginProgress.setVisibility(View.VISIBLE);
            login();
        }
    }

    public void toRegister() {
        WireframeClass.getInstance().toRegisterView(LoginView.this);
    }

    public void toLupaPassword() {
        WireframeClass.getInstance().toLupaPasswordView(LoginView.this);
    }

    void login() {

        String username = content.username.getText().toString();
        String password = content.password.getText().toString();

        ApiService apiService = ServiceGenerator.createService(ApiService.class, username, password);

        Call<Login> call = apiService.login(username, password);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                content.loginProgress.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Success success = response.body().getSuccess();

                    String id = success.getIduser();
                    String namauser = success.getNamauser();

                    if (BuildConfig.DEBUG) {
                        Log.i("Login Tag", "id = " + id);
                        Log.i("Login Tag", "namauser = " + namauser);
                    }

                    Toast.makeText(LoginView.this, "id : " + id + "\nNama User : "+ namauser, Toast.LENGTH_SHORT).show();

                } else {
                    if (BuildConfig.DEBUG) {
                        Log.e("Login Tag", "Error response : " + response.headers());
                        Log.e("Login Tag", "Error response : " + response.code());
                    }
                    Toast.makeText(LoginView.this, "Maaf anda tidak bisa login!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                content.loginProgress.setVisibility(View.GONE);
                Toast.makeText(LoginView.this, "Ada kesalah di jaringan anda! \n" + t.getMessage(), Toast.LENGTH_LONG).show();
                if (BuildConfig.DEBUG) {
                    Log.e("TagLogin", "Error retrofit : " + t.getMessage());
                }
            }
        });
    }
}

