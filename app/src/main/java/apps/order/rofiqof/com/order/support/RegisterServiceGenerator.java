package apps.order.rofiqof.com.order.support;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rofiqoff on 9/19/17.
 */

public class RegisterServiceGenerator {

    public static final String API_BASE_URL = Base.BASE_URL;

    // Here we add an interceptor in order to be able to listen the most typical messages
    private static BasicAuthInterceptor interceptor = new BasicAuthInterceptor();
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();//.addInterceptor(myLoggingInterceptor);

    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(API_BASE_URL).addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
}
