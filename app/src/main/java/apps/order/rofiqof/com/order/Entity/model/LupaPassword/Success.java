package apps.order.rofiqof.com.order.Entity.model.LupaPassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rofiqoff on 9/20/17.
 */

public class Success {
    @SerializedName("pesan")
    @Expose
    private String pesan;

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
