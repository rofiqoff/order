package apps.order.rofiqof.com.order.view;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

import apps.order.rofiqof.com.order.BuildConfig;
import apps.order.rofiqof.com.order.Entity.model.Register.Register;
import apps.order.rofiqof.com.order.R;
import apps.order.rofiqof.com.order.databinding.ActivityRegisterViewBinding;
import apps.order.rofiqof.com.order.modules.WireframeClass;
import apps.order.rofiqof.com.order.utils.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterView extends AppCompatActivity implements LocationListener {

    ActivityRegisterViewBinding content;
    ApiService apiService;

    public static final String TAG = "RegisterLOG";
    public static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;

    double latitude, longitude;

    String noHp, email, alamat, propinsi, namauser, password, imei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_register_view);
        content.setView(this);

        apiService = ApiService.Factory.create();

    }

    public void registerClick() {
        noHp = content.handphone.getText().toString();
        email = content.email.getText().toString();
        alamat = content.alamat.getText().toString();
        propinsi = content.propinsi.getText().toString();
        namauser = content.username.getText().toString();
        password = content.password.getText().toString();

        if (noHp.equalsIgnoreCase("")) {
            Toast.makeText(RegisterView.this, "No Hp Tidak boleh kososng!", Toast.LENGTH_SHORT).show();
        } else if (email.equalsIgnoreCase("")) {
            Toast.makeText(RegisterView.this, "Email Tidak boleh kososng!", Toast.LENGTH_SHORT).show();
        } else if (alamat.equalsIgnoreCase("")){
            Toast.makeText(RegisterView.this, "Alamat Tidak boleh kososng!", Toast.LENGTH_SHORT).show();
        } else if (propinsi.equalsIgnoreCase("")) {
            Toast.makeText(RegisterView.this, "Propinsi Tidak boleh kososng!", Toast.LENGTH_SHORT).show();
        } else if (namauser.equalsIgnoreCase("")) {
            Toast.makeText(RegisterView.this, "Namauser Tidak boleh kososng!", Toast.LENGTH_SHORT).show();
        } else if (password.equalsIgnoreCase("")) {
            Toast.makeText(RegisterView.this, "Password Tidak boleh kososng!", Toast.LENGTH_SHORT).show();
        } else {
            content.loginProgress.setVisibility(View.VISIBLE);
            registerData();
        }
    }

    void registerData() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
            } else {
                getDeviceImei();
            }
        }

        noHp = content.handphone.getText().toString();
        email = content.email.getText().toString();
        alamat = content.alamat.getText().toString();
        propinsi = content.propinsi.getText().toString();
        namauser = content.username.getText().toString();
        password = content.password.getText().toString();

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        imei = manager.getDeviceId();
//        imei = "Abkmn9984mm7";

        float lat = Float.parseFloat(String.valueOf(latitude));
        float lng = Float.parseFloat(String.valueOf(longitude));

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Hp : " + noHp);
            Log.d(TAG, "Email : " + email);
            Log.d(TAG, "Alamat : " + alamat);
            Log.d(TAG, "Propinsi : " + propinsi);
            Log.d(TAG, "Nama User : " + namauser);
            Log.d(TAG, "Password : " + password);
            Log.d(TAG, "Lat : " + lat);
            Log.d(TAG, "Lng : " + lng);
            Log.d(TAG, "imei : " + imei);
        }

        Call<Register> register = apiService.register(noHp, password, imei, email, (float) latitude, (float) longitude, alamat, propinsi, namauser);
        register.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                content.loginProgress.setVisibility(View.GONE);

                Log.d("LT","Berhasil!");
                if ( response.isSuccessful() ) {
                    Log.d("LT", "Response successful. Code: "+ response.code() + " " + response.headers());

                    Toast.makeText(RegisterView.this, "Pendaftaran Berhasil", Toast.LENGTH_SHORT).show();
                    WireframeClass.getInstance().toLoginView(RegisterView.this);

                    Register decodedResponseBody = response.body();

                    if ( decodedResponseBody != null ) {
                        Log.d("LT", "decodedResponseBody :"+decodedResponseBody+" decodedResponseBody.content-type: "+decodedResponseBody+" string: "+decodedResponseBody.toString());
                    } else {
                        Log.d("LT", "responseBody == null");
                    }

                } else {
                    try {

                        Log.d("LT", "response not successful: error: (" + response.errorBody().string()+") message: ("+response.message()+")");

                        if ( response != null ) {
                            Log.d("LT", "response != null ");
                            Register decodedResponseBody = response.body();
                            Toast.makeText(RegisterView.this, "Pendaftaran gagal!\n"+response.body(), Toast.LENGTH_SHORT).show();
                            Log.d("LT", "Response Body: "+ decodedResponseBody+" raw: "+response.raw() + " headers: " + response.headers());
                        } else {
                            Log.d("LT", "response == null");
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
                content.loginProgress.setVisibility(View.GONE);

                Toast.makeText(RegisterView.this, "Maaf Internet Anda bermasalah! \n"+t.getMessage(), Toast.LENGTH_SHORT).show();
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "kesalahan register : " + t.getMessage());
                }
            }
        });
    }

    void getDeviceImei() {
        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        imei = manager.getDeviceId();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "imei : "+imei);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getDeviceImei();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
