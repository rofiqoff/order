package apps.order.rofiqof.com.order.modules;

import android.content.Context;
import android.content.Intent;

import apps.order.rofiqof.com.order.view.LoginView;
import apps.order.rofiqof.com.order.view.LupaPasswordView;
import apps.order.rofiqof.com.order.view.RegisterView;

/**
 * Created by rofiqoff on 9/20/17.
 */

public class WireframeClass {
    private WireframeClass() {

    }

    public static class SigleToHelper {
        private static final WireframeClass INSTANCE = new WireframeClass();
    }

    public static WireframeClass getInstance() {
        return SigleToHelper.INSTANCE;
    }

    public void toLoginView(Context context) {
        Intent intent = new Intent(context, LoginView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public void toRegisterView(Context context) {
        Intent intent = new Intent(context, RegisterView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public void toLupaPasswordView(Context context) {
        Intent intent = new Intent(context, LupaPasswordView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}
