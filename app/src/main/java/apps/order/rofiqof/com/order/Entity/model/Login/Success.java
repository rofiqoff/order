package apps.order.rofiqof.com.order.Entity.model.Login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rofiqoff on 9/18/17.
 */

public class Success {
    @SerializedName("iduser")
    @Expose
    private String iduser;
    @SerializedName("namauser")
    @Expose
    private String namauser;
    @SerializedName("nohandphone")
    @Expose
    private String nohandphone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("deposit")
    @Expose
    private Integer deposit;
    @SerializedName("point")
    @Expose
    private Integer point;
    @SerializedName("token")
    @Expose
    private String token;

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getNamauser() {
        return namauser;
    }

    public void setNamauser(String namauser) {
        this.namauser = namauser;
    }

    public String getNohandphone() {
        return nohandphone;
    }

    public void setNohandphone(String nohandphone) {
        this.nohandphone = nohandphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getDeposit() {
        return deposit;
    }

    public void setDeposit(Integer deposit) {
        this.deposit = deposit;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
