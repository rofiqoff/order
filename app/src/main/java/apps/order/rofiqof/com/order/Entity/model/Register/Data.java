package apps.order.rofiqof.com.order.Entity.model.Register;

/**
 * Created by rofiqoff on 9/19/17.
 */

public class Data {
    String nohandphone;
    String password;
    String imei;
    String email;
    String latitudeasli;
    String longitudeasli;
    String alamat;
    String propinsi;
    String namauser;

    public Data(String nohandphone, String password, String imei, String email, String latitudeasli, String longitudeasli, String alamat, String propinsi, String namauser) {
        this.nohandphone = nohandphone;
        this.password = password;
        this.imei = imei;
        this.email = email;
        this.latitudeasli = latitudeasli;
        this.longitudeasli = longitudeasli;
        this.alamat = alamat;
        this.propinsi = propinsi;
        this.namauser = namauser;
    }
}
