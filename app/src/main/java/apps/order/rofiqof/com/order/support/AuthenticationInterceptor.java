package apps.order.rofiqof.com.order.support;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by rofiqoff on 9/18/17.
 */

public class AuthenticationInterceptor implements Interceptor {

    String authToken;

    public AuthenticationInterceptor(String token) {
        this.authToken = token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request request = original.newBuilder()
                .header("Content-Type", "application/json")
                .header("Authorization", authToken)
                .method(original.method(), original.body())
                .build();
        return chain.proceed(request);
    }
}
