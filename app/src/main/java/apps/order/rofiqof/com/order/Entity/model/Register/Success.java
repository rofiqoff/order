package apps.order.rofiqof.com.order.Entity.model.Register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rofiqoff on 9/17/17.
 */

public class Success {

    @SerializedName("iduser")
    @Expose
    private String iduser;
    @SerializedName("nohandphone")
    @Expose
    private String nohandphone;
    @SerializedName("namauser")
    @Expose
    private String namauser;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("propinsi")
    @Expose
    private String propinsi;
    @SerializedName("latitudeasli")
    @Expose
    private String latitudeasli;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("deposit")
    @Expose
    private Integer deposit;
    @SerializedName("point")
    @Expose
    private Integer point;
    @SerializedName("statususer")
    @Expose
    private Integer statususer;
    @SerializedName("token")
    @Expose
    private String token;

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getNohandphone() {
        return nohandphone;
    }

    public void setNohandphone(String nohandphone) {
        this.nohandphone = nohandphone;
    }

    public String getNamauser() {
        return namauser;
    }

    public void setNamauser(String namauser) {
        this.namauser = namauser;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public String getLatitudeasli() {
        return latitudeasli;
    }

    public void setLatitudeasli(String latitudeasli) {
        this.latitudeasli = latitudeasli;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getDeposit() {
        return deposit;
    }

    public void setDeposit(Integer deposit) {
        this.deposit = deposit;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Integer getStatususer() {
        return statususer;
    }

    public void setStatususer(Integer statususer) {
        this.statususer = statususer;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
