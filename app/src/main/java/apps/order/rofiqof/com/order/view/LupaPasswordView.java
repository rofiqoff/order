package apps.order.rofiqof.com.order.view;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

import apps.order.rofiqof.com.order.BuildConfig;
import apps.order.rofiqof.com.order.Entity.model.LupaPassword.LupaPassword;
import apps.order.rofiqof.com.order.R;
import apps.order.rofiqof.com.order.databinding.ActivityLupaPasswordViewBinding;
import apps.order.rofiqof.com.order.modules.WireframeClass;
import apps.order.rofiqof.com.order.utils.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LupaPasswordView extends AppCompatActivity {

    public static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;

    ActivityLupaPasswordViewBinding content;
    ApiService apiService;

    private static final String TAG = "LupaPasswordLOG";

    String username, password, imei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_lupa_password_view);
        content.setView(this);

        apiService = ApiService.Factory.create();
    }

    public void gantiPasswordClick() {
        if (content.username.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(LupaPasswordView.this, "Username tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else if (content.passwordBaru.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(LupaPasswordView.this, "Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else {
            content.progressBar.setVisibility(View.VISIBLE);
            gantiPassword();
        }
    }

    void gantiPassword() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
                getDeviceImei();
            } else {
                getDeviceImei();
            }
        }

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        imei = manager.getDeviceId();

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "username : " + content.username.getText().toString());
            Log.d(TAG, "password baru : " + content.passwordBaru.getText().toString());
            Log.d(TAG, "imei : " + imei);
        }

        Call<LupaPassword> lupaPassword = apiService.gantiPassword(content.username.getText().toString(), imei, content.passwordBaru.getText().toString());
        lupaPassword.enqueue(new Callback<LupaPassword>() {
            @Override
            public void onResponse(Call<LupaPassword> call, Response<LupaPassword> response) {
                content.progressBar.setVisibility(View.GONE);

                Log.d(TAG, "Berhasil");
                if (response.isSuccessful()) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Response successfull. Code : " + response.code() + " " + response.headers());
                    }

                    LupaPassword decodedResponseBody = response.body();

                    if ( decodedResponseBody != null ) {
                        Log.d(TAG, "decodedResponseBody :"+decodedResponseBody+" decodedResponseBody.content-type: "+decodedResponseBody+" string: "+decodedResponseBody.toString());
                    } else {
                        Log.d(TAG, "responseBody == null");
                    }

                    Toast.makeText(LupaPasswordView.this, "Success!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(LupaPasswordView.this, response.body().getSuccess().getPesan(), Toast.LENGTH_SHORT).show();
                    WireframeClass.getInstance().toLoginView(LupaPasswordView.this);

                } else {
                    try {
                        Log.d(TAG, "response not successful: error: (" + response.errorBody().string()+") message: ("+response.message()+")");

                        if ( response != null ) {
                            LupaPassword decodedResponseBody = response.body();

                            Log.d(TAG, "response != null ");
                            Log.d(TAG, "Response Body: "+ decodedResponseBody+" raw: "+response.raw() + " headers: " + response.headers());

                            Toast.makeText(LupaPasswordView.this, "Ganti Password gagal!\n"+response.body(), Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d(TAG, "response == null");
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LupaPassword> call, Throwable t) {
                content.progressBar.setVisibility(View.GONE);

                Toast.makeText(LupaPasswordView.this, "Maaf Internet Anda bermasalah! \n"+t.getMessage(), Toast.LENGTH_SHORT).show();
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "kesalahan : " + t.getMessage());
                }
            }
        });
    }

    private void getDeviceImei() {
        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        imei = manager.getDeviceId();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "imei : "+imei);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getDeviceImei();
        }
    }
}
